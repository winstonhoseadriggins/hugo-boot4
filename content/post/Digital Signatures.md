+++
title = "Blockchain Transactions"
date = "2017-02-26T13:47:08+02:00"
tags = ["gas","ether", "contract", "calls", "public", "key", "encryption"]
categories = ["programming"]
banner = "images/image01.jpg"
+++

## BLOCKCHAIN TRANSACTIONS

Transactions make changes to storage and have fixed transaction costs.
Transactions are executed on every node.
Transactions can only return results by events.
One Contract Transact On Another and get returned data.
TRANSACTION FAILURE ALL CHANGES REVERSED, NO LOG PRODUCED

Transactions can POST a Contract
Another transaction can call that contract.

## DIGITAL SIGNATURES

Hashing is the product of a small change in content which causes a huge change in results.

Public Key Encryption is the method to encrypt hash values.