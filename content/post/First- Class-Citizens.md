+++
title = "First Class Functions in Go"
date = "2017-08-08T13:39:46+02:00"
banner = "images/conferences.jpg"
+++
Andrew Gerrand

**Andrew Gerrand


Programmers new to Go are often surprised by its support for function types, functions as values, and closures. The [[http://golang.org/doc/codewalk/functions/][First Class Functions in Go]] code walk demonstrates these features with a simulation of the dice game [[http://en.wikipedia.org/wiki/Pig_(dice)][Pig]]. It is a pretty program that uses the language to great effect, and a fun read for Go beginners and veterans alike.

More resources are available at [[http://golang.org/doc/docs.html][golang.org]].
