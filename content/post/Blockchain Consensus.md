+++
title = "Blockchain Benefits"
date = "2017-06-26T13:47:08+02:00"
tags = ["go","etherium"]
categories = ["programming","blockchain"]
banner = "images/image04.jpg"
+++

## BLOCKCHAIN BENEFITS

An immutable record system with role restricted activities, as well as Publically Auditable Data


## BLOCKCHAIN DEFINITION

A Blockchain consist of signed transactions put into signed blocks which are added to the chain in a manner such that all the independant nodes that operate the network agree the whole transaction valid. 



## BLOCKCHAIN CONSENSUS

Design of consensus algorithm ensures that No One Node makes all the decisions.
Makes it virtually impossible for anyone to modify past records in Blockchain.
Math and Computer Science guarantee ownership and scarcity